<?php
/**
 * Created by PhpStorm.
 * User: karamush
 * Date: 13.07.2018
 * Time: 23:08
 */

class MorseCoder_Class {

	public $MORSE_CHAR_DELIMITER = ' ';            // разделитель между символами в обычной морзянке
	public $UNKNOWN_CHAR = '?';                    // это если перевести не удалось. неизвестный символ
	public $MORSE_DOT = '.';                       // точка
	public $MORSE_DASH = '-';                      // тире
	public $MORSE_BIN_DOT = '1';                   // бинарная точка
	public $MORSE_BIN_DASH ='111';                 // бинарное тире
	public $MORSE_BIN_DELIMITER = '0';             // разделитель между знаками одного символа
	public $MORSE_BIN_CHAR_DELIMITER = '00';       // разделитель между символами
	public $MORSE_BIN_WORD_DELIMITER = '000';      // разделитель между словами (эм. пробел?)

	const MORSE_TYPE_SIMPLE = 0;
	const MORSE_TYPE_BINARY = 1;
	const MORSE_LANGUAGE_LATIN = 1;
	const MORSE_LANGUAGE_CYRILLIC = 0;

	public $morse_type = self::MORSE_TYPE_BINARY;
	public $morse_language = self::MORSE_LANGUAGE_CYRILLIC;
	public $morse_capital = false;

	// символы и цифры
	private $codes_symbols = [
		'1' => '.----',
		'2' => '..---',
		'3' => '...--',
		'4' => '....-',
		'5' => '.....',
		'6' => '-....',
		'7' => '--...',
		'8' => '---..',
		'9' => '----.',
		'0' => '-----',
		'.' => '......',
		',' => '.-.-.-',
		':' => '---...',
		';' => '-.-.-.',
		'(' => '-.--.-',
		')' => '-.--..', // TODO: использовал собственный символ для закрывающей скобки. опасно :)
		'\'' => '.----.',
		'`' => '.----.',
		'"' => '.-..-.',
		'-' => '-....-',
		'/' => '-..-.',
		'?' => '..--..',
		'!' => '--..--',
		' ' => '-...-', // знак раздела, пробел, короче, WTF?!
		"\n" => '-.-..',    // TODO: ещё один добавленный символ. Символ переноса строки!
		"\r" => '-.-..-',   // TODO: ай-ай-ай!
		'@' => '.--.-.',
		'[END]' => '..-.-'
	];

	// латинский алфавит
	private $codes_latin = [
		'A' => '.-',
		'B' => '-...',
		'C' => '-.-.',
		'D' => '-..',
		'E' => '.',
		'F' => '..-.',
		'G' => '--.',
		'H' => '....',
		'I' => '..',
		'J' => '.---',
		'K' => '-.-',
		'L' => '.-..',
		'M' => '--',
		'N' => '-.',
		'O' => '---',
		'P' => '.--.',
		'Q' => '--.-',
		'R' => '.-.',
		'S' => '...',
		'T' => '-',
		'U' => '..-',
		'W' => '.--',
		'X' => '-..-',
		'Y' => '-.--',
		'Z' => '--..',
		'V' => '...-',
		'CH' => '---.',
		'SH' => '----',
		'Ñ' => '-..-',
		'É' => '..-..',
		'Ü' => '..--',
		'Ä' => '.-.-'
	];

	// кириллический алфавит
	private $codes_cyrillic = [
		'А' => '.-',
		'Б' => '-...',
		'В' => '.--',
		'Г' => '--.',
		'Д' => '-..',
		'Е' => '.',     'Ё' => '.',
		'Ж' => '...-',
		'З' => '--..',
		'И' => '..',
		'Й' => '.---',
		'К' => '-.-',
		'Л' => '.-..',
		'М' => '--',
		'Н' => '-.',
		'О' => '---',
		'П' => '.--.',
		'Р' => '.-.',
		'С' => '...',
		'Т' => '-',
		'У' => '..-',
		'Ф' => '..-.',
		'Х' => '....',
		'Ц' => '-.-.',
		'Ч' => '---.',
		'Ш' => '----',
		'Щ' => '--.-',
		'Ь' => '-..-',  'Ъ' => '-..-',
		'Ы' => '-.--',
		'Э' => '..-..',
		'Ю' => '..--',
		'Я' => '.-.-',
	];

	public function __construct() {

	}

	/**
	 * Получить текущий полный алфавит, но с приоритетом языка,
	 * чтоб в первую очередь шифровалось или дешифровалось на текущем языке,
	 * но отсутствующие символы чтобы тоже были. Плюс цифры и другие символы.
	 *
	 * @return array
	 */
	private function get_current_full_alphabet() {
		$alphabet = ($this->morse_language == self::MORSE_LANGUAGE_CYRILLIC) ? array_merge($this->codes_cyrillic, $this->codes_latin) : array_merge($this->codes_latin, $this->codes_cyrillic);
		return array_merge($alphabet, $this->codes_symbols);
	}

	/**
	 * Переводит обычный символ в его Морзе-символ
	 * @param $char
	 *
	 * @return mixed|string
	 */
	function char_to_morse($char) {
		$char = mb_strtoupper($char);
		return array_key_exists($char, $this->get_current_full_alphabet()) ? $this->get_current_full_alphabet()[$char] : $this->UNKNOWN_CHAR;
	}

	/**
	 * Приводит строку с морзянкой к общему формату, заменяя звёздочки на точки и т.д.
	 * @param $morse_text
	 *
	 * @return string
	 */
	function prepare_morse_text($morse_text) {
		$morse_text = str_replace('*', '.', $morse_text);
		$morse_text = str_replace('_', '-', $morse_text);
		$morse_text = preg_replace('/' . $this->MORSE_CHAR_DELIMITER . '{2,}/',$this->MORSE_CHAR_DELIMITER, $morse_text);
        return $morse_text;
	}

	function mbStringToArray($string, $encoding = 'UTF-8') {
		$strlen = mb_strlen($string);
		$str_array = [];
		while ($strlen) {
			$str_array[] = mb_substr($string, 0, 1, $encoding);
			$string = mb_substr($string, 1, $strlen, $encoding);
			$strlen = mb_strlen($string, $encoding);
		}
		return $str_array;
	}

	/**
	 * Переводит Морзе-код символа в его обычный символ
	 * @param $morse_char
	 *
	 * @return string
	 */
	function morse_to_char($morse_char) {
		return in_array($morse_char, $this->get_current_full_alphabet()) ? array_search($morse_char, $this->get_current_full_alphabet()) : $this->UNKNOWN_CHAR;
	}

	/**
	 * Переводит строку обычного текста в морзянку, ставя также разделитель после каждого символа.
	 * @param $plain_text
	 *
	 * @return string
	 */
	function text_to_morse($plain_text) {
		$plain_text = $this->mbStringToArray(mb_strtoupper($plain_text));
		$result = '';
		foreach ($plain_text as $char) {
			$result .= $this->char_to_morse($char) . $this->MORSE_CHAR_DELIMITER;
		}
		return $result;
	}

	/**
	 * Переводит морзянку в обычный человеческий текст.
	 *
	 * @param      $morse_text
	 *
	 * @return string
	 */
	function morse_to_text($morse_text) {
        // сначала к единому формату тире и точек привести нужно символ
	    $morse_text = $this->prepare_morse_text($morse_text);
		$morse_chars = explode($this->MORSE_CHAR_DELIMITER, $morse_text);
		$result = '';
		foreach($morse_chars as $char) {
			$result .= $this->morse_to_char($char);
		}

		return $this->morse_capital ? $result : mb_strtolower($result);
	}

	/**
	 * Переводит бинарную морзянку в морзянку обычную.
	 * @param $morse_bin_text
	 *
	 * @return string
	 */
	function morse_bin_to_morse($morse_bin_text) {
		$result = '';
		$bin_morse_chars = explode($this->MORSE_BIN_CHAR_DELIMITER, $morse_bin_text);
		foreach ($bin_morse_chars as $bin_morse_char) {
			$bin_morse_char = str_replace($this->MORSE_BIN_DASH, $this->MORSE_DASH, $bin_morse_char);
			$bin_morse_char = str_replace($this->MORSE_BIN_DOT, $this->MORSE_DOT, $bin_morse_char);
			$bin_morse_char = str_replace($this->MORSE_BIN_DELIMITER, '', $bin_morse_char);
			$result .= $bin_morse_char . $this->MORSE_CHAR_DELIMITER;
		}
		return rtrim($result, $this->MORSE_CHAR_DELIMITER);
	}

	/**
	 * Переводит морзянку в бинарную морзянку.
	 * @param $morse_text
	 *
	 * @return string
	 */
	function morse_to_morse_bin($morse_text) {
		$result = '';
		$morse_text = $this->prepare_morse_text($morse_text);
		for($i=0; $i<strlen($morse_text);$i++) {
			switch ($morse_text[$i]) {
				case $this->MORSE_DOT: $result .= $this->MORSE_BIN_DOT . $this->MORSE_BIN_DELIMITER; break;
				case $this->MORSE_DASH: $result .= $this->MORSE_BIN_DASH . $this->MORSE_BIN_DELIMITER; break;
				case $this->MORSE_CHAR_DELIMITER: $result .= $this->MORSE_BIN_DELIMITER; break; // добавляем ещё один нолик, а не два нуля. Потому что с предыдущего символа нолик ещё есть
			}
		}
		return $result;
	}

	/**
	 * Текст в бинарную морзянку
	 * @param $plaint_text
	 *
	 * @return string
	 */
	function text_to_morse_bin($plaint_text) {
		return $this->morse_to_morse_bin($this->text_to_morse($plaint_text));
	}

	/**
	 * Бинарную морзянку в текст
	 *
	 * @param      $morse_bin
	 *
	 * @return string
	 */
	function morse_bin_to_text($morse_bin) {
		return $this->morse_to_text($this->morse_bin_to_morse($morse_bin));
	}

	/**
	 * Закодировать открытый текст в бинарную морзянку или в обычную
	 * (зависит от параметра morse_type)
	 * @param $plain_text
	 *
	 * @return string
	 */
	function morse_encode($plain_text) {
		return ($this->morse_type == self::MORSE_TYPE_BINARY) ? $this->text_to_morse_bin($plain_text) : $this->text_to_morse($plain_text);
	}

	/**
	 * Раскодировать закодированный текст из бинарной морзянки или из обычной
	 * (зависит от параметра morse_type)
	 * @param $encoded_text
	 *
	 * @return string
	 */
	function morse_decode($encoded_text) {
		return ($this->morse_type == self::MORSE_TYPE_BINARY) ? $this->morse_bin_to_text($encoded_text) : $this->morse_to_text($encoded_text);
	}


}
