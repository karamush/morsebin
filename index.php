<?php
$start_time = microtime(true);

require_once __DIR__ . '/MorseCoder_Class.php';
$morse = new MorseCoder_Class();

function post($arg_name, $default_value = null) {
    return isset($_POST[$arg_name]) ? trim($_POST[$arg_name]) : $default_value;
}

function post_if_then($arg_name, $query_value, $if_yes = null, $if_no = null) {
    return post($arg_name) == $query_value ? $if_yes : $if_no;
}

$input_text = post('input');

$result = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if (mb_strlen($input_text) > 16000) {
		exit('Сорян, но слишком большой входной текст... )');
	}

	$morse->morse_language = post_if_then('language', 'cyrillic', MorseCoder_Class::MORSE_LANGUAGE_CYRILLIC, MorseCoder_Class::MORSE_LANGUAGE_LATIN);
	$morse->morse_capital = (bool)post('caps', true);
	$morse->morse_type = post_if_then('morse_type', 'morsebin', MorseCoder_Class::MORSE_TYPE_BINARY, MorseCoder_Class::MORSE_TYPE_SIMPLE);

	if (isset($_POST['encode'])) { // кодирование
		$result = $morse->morse_encode($input_text);
	}

	if (isset($_POST['decode'])) {
		$result = $morse->morse_decode($input_text);
	}

}
?><!doctype html>
<html lang="ru">
<head>
    <title>Бинарная азбука Морзе онлайн</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Онлайн кодирование и декодирование азбуки Морзе двоичным кодом">
    <meta name="keywords"
          content="Морзе, Азбука, двоичный код, морзянка, кодировка, расшифровка, двоичная азбука Морзе">
    <meta name="author" content="karamush">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <link rel="apple-touch-icon" sizes="180x180" href="static/img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="static/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="64x64" href="static/img/favicons/favicon-64x64.png">
    <link rel="icon" type="image/png" sizes="16x16" href="static/img/favicons/favicon-16x16.png">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" href="static/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="MorseBin">
    <meta name="application-name" content="MorseBin">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <script>
		if (location.protocol !== 'https:') {
			location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
		}
    </script>

    <style>
        div.convert {
            color: rgba(0, 0, 0, 1);
            width: auto;
            height: auto;
            margin: 4px 0 0 2px;
            padding: 12px;
            display: block;
            position: relative;
            overflow: hidden;
            font-size: 13px;
            font-style: normal;
            text-align: justify;
            font-family: 'Arial';
            font-weight: normal;
            line-height: normal;
            background-color: rgba(255, 255, 255, 1);
        }

        div.convert > h1 {
            color: rgba(0, 0, 0, 1);
            width: auto;
            height: auto;
            margin: 2px 0 0 0;
            padding: 0;
            display: block;
            position: relative;
            font-size: 20px;
            font-style: normal;
            text-align: justify;
            font-family: 'Arial';
            font-weight: normal;
            line-height: normal;
            text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.1)
        }

        div.convert > p {
            color: rgba(96, 96, 96, 1);
            width: auto;
            height: auto;
            margin: 0 0 10px 0;
            padding: 3px 0 7px 0;
            display: block;
            position: relative;
            font-size: 12px;
            font-style: normal;
            text-align: justify;
            font-family: 'Arial';
            font-weight: normal;
            line-height: normal;
            border-bottom: 1px dashed rgba(204, 204, 204, 1)
        }

        div.convert a {
            color: rgba(96, 96, 96, 1);
            text-decoration: none
        }

        div.convert a:hover {
            text-decoration: underline
        }

        div.convert textarea {
            color: rgba(0, 0, 0, 1);
            width: 100%;
            height: 150px;
            border: 1px solid rgba(204, 204, 204, 1);
            margin: 0;
            padding: 4px;
            display: block;
            position: relative;
            overflow: auto;
            font-size: 13px;
            box-sizing: border-box;
            font-style: normal;
            text-align: left;
            font-family: 'Arial';
            font-weight: normal;
            line-height: normal;
            background-color: rgba(238, 238, 238, 1);
        }

        #output {
            margin-top: 6px;
        }

        #decode-btn {
            background-color: rgb(72, 144, 1);
        }

        #encode-btn {
            background-color: rgb(72, 144, 1);
        }

        #btn_swap_codes {
            background-color: rgb(3, 182, 100);
        }

        #btn_copy_result {
            background-color: rgb(3, 182, 100);
        }

        #c_form button:hover {
            color: #000;
        }

        div#convert_text button {
            float: left;
            color: rgba(255, 255, 255, 1);
            width: 144px;
            height: 24px;
            cursor: pointer;
            border: 1px solid rgba(67, 154, 0, 1);
            padding: 2px;
            display: block;
            position: relative;
            font-size: 13px;
            font-style: normal;
            text-align: center;
            font-family: 'Arial';
            font-weight: bold;
            text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.2);
            margin: 4px 10px 4px 0;
        }

        div#convert_text button i.fa:first-of-type {
            margin-right: 4px
        }

        div#convert_text button i.fa:last-of-type {
            margin-left: 4px
        }

        div#convert_text span {
            float: left;
            color: rgba(96, 96, 96, 1);
            width: auto;
            height: 24px;
            margin: 4px 0 0 0;
            padding: 0;
            display: block;
            position: relative;
            font-size: 12px;
            font-style: normal;
            text-align: center;
            font-family: 'Arial';
            font-weight: normal;
            line-height: 24px
        }

        div.convert > span {
            color: rgba(68, 68, 68, 1);
            width: auto;
            height: 18px;
            margin: 10px 0 0 0;
            padding: 0 8px;
            display: inline-block;
            position: relative;
            font-size: 11px;
            font-style: normal;
            text-align: left;
            font-family: 'Arial';
            font-weight: normal;
            line-height: 18px;
            border-top: 1px solid rgba(204, 204, 204, 1);
            border-left: 1px solid rgba(204, 204, 204, 1);
            border-right: 1px solid rgba(204, 204, 204, 1)
        }

        div.convert > span i.fa {
            margin-right: 4px
        }

        div.convert > div.wiki {
            color: rgba(68, 68, 68, 1);
            width: 100%;
            height: 240px;
            border: 1px dashed rgba(204, 204, 204, 1);
            margin: 0;
            padding: 4px;
            display: block;
            position: relative;
            overflow: auto;
            font-size: 11px;
            box-sizing: border-box;
            font-style: normal;
            text-align: justify;
            font-family: 'Arial';
            font-weight: normal;
            line-height: normal
        }

        div.convert > div.wiki a {
            color: rgba(67, 154, 0, 1);
            text-decoration: none
        }

        div.convert > div.wiki a:hover {
            text-decoration: underline
        }

        div.convert > div.wiki table td, div.convert > div.wiki table th {
            text-align: center;
            background-color: rgba(238, 238, 238, 1)
        }

        div.wiki table {
            font-size: 13px;
        }

        a, ul, li, img, button, select, textarea {
            resize: none;
            outline: 0;
            -webkit-tap-highlight-color: transparent;
        }

        div#convert_text select {
            float: left;
            color: rgba(0, 0, 0, 1);
            width: 100px;
            height: 24px;
            cursor: pointer;
            border: 1px solid rgba(204, 204, 204, 1);
            margin: 4px 4px 0 0;
            padding: 0;
            display: block;
            position: relative;
            font-size: 13px;
            font-style: normal;
            text-align: center;
            font-family: 'Arial';
            font-weight: normal;
            line-height: 24px;
            background-color: rgba(238, 238, 238, 1);
        }
    </style>
</head>

<body>
<div class="convert">
    <h1>Азбука морзе двоичным кодом</h1>
    <p>Простое кодирование и декодирование азбуки Морзе, используя двоичный код</p>

    <div id="convert_text">
        <form method="post" name="c_form" id="c_form">
            <textarea id="input" name="input"
                      placeholder="Набери или вставь исходный текст сюда"><?php echo $input_text; ?></textarea>
            <button type="submit" name="encode" value="encode" id="encode-btn"><i class="fa fa-lock"></i>Закодировать
            </button>
            <button type="submit" name="decode" value="decode" id="decode-btn"><i class="fa fa-unlock-alt"></i>Раскодировать
            </button>
            <button type="button" onclick="swap_codes(); return false;" name="btn_swap_codes" id="btn_swap_codes"
                    title="Поменять местами текст в полях"><i class="fa fa-arrows-alt-v"></i>Поменять местами
            </button>
            <button type="button" onclick="copy_result(); return false;" name="btn_copy_result" id="btn_copy_result"
                    title="Скопировать результат кодирования или декодирования в буфер обмена"><i
                        class="fa fa-copy"></i>Скопировать
            </button>
            <select name="morse_type" id="morse_type_select" title="Выбор типа кодирования и декодирования">
                <option value="morsebin" <?php if ($morse->morse_type == MorseCoder_Class::MORSE_TYPE_BINARY) echo 'selected'; ?>>Бинарная Морзе</option>
                <option value="morse" <?php if ($morse->morse_type == MorseCoder_Class::MORSE_TYPE_SIMPLE) echo 'selected'; ?>>Обычная Морзе</option>
            </select>
            <select name="language" id="language_select" title="Выбор языка ДЕКОДИРОВАНИЯ">
                <option value="cyrillic" <?php if ($morse->morse_language == MorseCoder_Class::MORSE_LANGUAGE_CYRILLIC) echo 'selected'; ?>>Кириллица</option>
                <option value="latin" <?php if ($morse->morse_language == MorseCoder_Class::MORSE_LANGUAGE_LATIN) echo 'selected'; ?>>Латиница</option>
            </select>
            <select name="caps" id="caps_select" title="Выбор регистра букв при ДЕКОДИРОВАНИИ">
                <option value="1" <?php if ($morse->morse_capital) echo 'selected'; ?>>ЗАГЛАВНЫЕ</option>
                <option value="0" <?php if (!$morse->morse_capital) echo 'selected'; ?>>строчные</option>
            </select>
        </form>
        <div class="clear"></div>
    </div>
    <textarea readonly id="output" name="output" placeholder="Здесь результат"><?php echo $result; ?></textarea>

    <span><i class="fa fa-question-circle"></i>О проекте</span>
    <div class="wiki">
        Здесь можно легко и просто закодировать текст в Азбуку Морзе, да не просто в обычные точки и тире, а ещё и в
        бинарный код!<br>
        <br>
        <b>Правила кодирования</b><br><br>
        Кодирование азбуки Морзе в бинарный код -- это просто замена точек и тире на единицы и нули, но по определённым правилам:
        <ul>
            <li>"Точка" кодируется цифрой <b>1</b></li>
            <li>"Тире" кодируется тремя единицами (ведь тире по длительности равно трём точкам)
            <li>Между точкой и тире используется разделитель: цифра <b>0</b></li>
            <li>Между каждым символом используется два нуля, так можно будет отличить, что начался следующий символ</li>
        </ul>
        <br>
        <b>Как пользоваться</b><br>
        <br>
        Исходный текст нужно набрать или вставить в верхнем поле, затем нажать кнопку "Закодировать", чтобы получить в
        нижнем поле закодированный текст. Режим кодирования (бинарный или обычный) можно выбрать в выпадающем списке.
        <br>Чтобы раскодировать, нужно опять же, вставить или набрать текст в верхнем поле и нажать "Раскодировать". Тогда внизу появится раскодированный текст.
        <br>
        Для удобства существует кнопка "Поменять местами", которая меняет содержимое полей местами, что избавляет от ручного
        копирования и вставки текста, если вдруг для проверки захочется раскодировать то, что было только что закодировано или если захочется ещё раз закодировать уже закодированное сообщение (а это возможно, да!).
        <br>
        Также имеются некоторые настройки, а именно:
            <ul>
                <li>Выбор режима кодирования и декодирования. Можно и в обычную азбуку Морзе, и в бинарную</li>
                <li>Выбор алфавита: кириллические символы или латинские. Если при расшифровки послания получился какой-то дикий транслит, возможно, стоит поменять алфавит</li>
                <li>Выбор регистра букв. Имеет смысл, как и предыдущий пункт, только при расшифровке. Выбор между БОЛЬШИМИ СТРАШНЫМИ И ВЫЗЫВАЮЩИМИ ПАНИКУ или тихими и спокойными строчными буквами</li>
            </ul>
        <br>
        Выбор определённого алфавита не отключает полностью другой. Поэтому, если попадутся символы, которых нет в указанном алфавите, то они поищутся ещё и в другом. Знаки препинания, цифры и другие символы находятся отдельно, поэтому выбор алфавита на них никак не влияет.
        <br>
        <i>Примечание. </i>Если попытаться раскодировать в бинарном режиме обычную азбуку Морзе, то, скорее всего, ничего не получится.
        <br>
        Символы, которые не удалось закодировать или раскодировать, будут помечены вопросительным знаком.
        <br><br>
        <b>Полностью бесплатно</b><br>
        <br>
        Данный сервис можно использовать совершенно бесплатно. Не нужно скачивать никакого дополнительного программного обеспечения для пользования этим сервисом.
        <br>
        <h3>Используемые алфавиты для кодирования и декодирования</h3>
        <i>Таблицы взяты из wikipedia, они же и используются для кодирования, но некоторые символы изменены</i>
        <br><br>
        <b>Буквы</b>
        <table>
            <tbody>
            <tr>
                <th>Кириллица</th>
                <th>Латиница</th>
                <th>Код Морзе</th>
            </tr>
            <tr>
                <td>А</td>
                <td>A</td>
                <td>.-</td>
            </tr>
            <tr>
                <td>Б</td>
                <td>B</td>
                <td>-...</td>
            </tr>
            <tr>
                <td>В</td>
                <td>W</td>
                <td>.--</td>
            </tr>
            <tr>
                <td>Г</td>
                <td>G</td>
                <td>--.</td>
            </tr>
            <tr>
                <td>Д</td>
                <td>D</td>
                <td>-..</td>
            </tr>
            <tr>
                <td>Е, Ё</td>
                <td>E</td>
                <td>.</td>
            </tr>
            <tr>
                <td>Ж</td>
                <td>V</td>
                <td>...-</td>
            </tr>
            <tr>
                <td>З</td>
                <td>Z</td>
                <td>--..</td>
            </tr>
            <tr>
                <td>И</td>
                <td>I</td>
                <td>..</td>
            </tr>
            <tr>
                <td>Й</td>
                <td>J</td>
                <td>.---</td>
            </tr>
            <tr>
                <td>К</td>
                <td>K</td>
                <td>-.-</td>
            </tr>
            <tr>
                <td>Л</td>
                <td>L</td>
                <td>.-..</td>
            </tr>
            <tr>
                <td>М</td>
                <td>M</td>
                <td>--</td>
            </tr>
            <tr>
                <td>Н</td>
                <td>N</td>
                <td>-.</td>
            </tr>
            <tr>
                <td>О</td>
                <td>O</td>
                <td>---</td>
            </tr>
            <tr>
                <td>П</td>
                <td>P</td>
                <td>.--.</td>
            </tr>
            <tr>
                <td>Р</td>
                <td>R</td>
                <td>.-.</td>
            </tr>
            <tr>
                <td>С</td>
                <td>S</td>
                <td>...</td>
            </tr>
            <tr>
                <td>Т</td>
                <td>T</td>
                <td>-</td>
            </tr>
            <tr>
                <td>У</td>
                <td>U</td>
                <td>..-</td>
            </tr>
            <tr>
                <td>Ф</td>
                <td>F</td>
                <td>..-.</td>
            </tr>
            <tr>
                <td>Х</td>
                <td>H</td>
                <td>....</td>
            </tr>
            <tr>
                <td>Ц</td>
                <td>C</td>
                <td>-.-.</td>
            </tr>
            <tr>
                <td>Ч</td>
                <td>CH</td>
                <td>---.</td>
            </tr>
            <tr>
                <td>Ш</td>
                <td>SH</td>
                <td>----</td>
            </tr>
            <tr>
                <td>Щ</td>
                <td>Q</td>
                <td>--.-</td>
            </tr>
            <tr>
                <td>Ъ</td>
                <td>Ñ</td>
                <td>--.--</td>
            </tr>
            <tr>
                <td>Ы</td>
                <td>Y</td>
                <td>-.--</td>
            </tr>
            <tr>
                <td>Ь, Ъ</td>
                <td>X</td>
                <td>-..-</td>
            </tr>
            <tr>
                <td>Э</td>
                <td>É</td>
                <td>..-..</td>
            </tr>
            <tr>
                <td>Ю</td>
                <td>Ü</td>
                <td>..--</td>
            </tr>
            <tr>
                <td>Я</td>
                <td>Ä</td>
                <td>.-.-</td>
            </tr>
            </tbody>
        </table>
        <br>

        <b>Цифры и символы</b>
        <table>
            <tbody>
            <tr>
                <th>Символ</th>
                <th>Код</th>
            </tr>
            <tr>
                <td>1</td>
                <td>.----</td>
            </tr>
            <tr>
                <td>2</td>
                <td>..---</td>
            </tr>
            <tr>
                <td>3</td>
                <td>...--</td>
            </tr><tr>
                <td>4</td>
                <td>....-</td>
            </tr><tr>
                <td>5</td>
                <td>.....</td>
            </tr><tr>
                <td>6</td>
                <td>-....</td>
            </tr><tr>
                <td>7</td>
                <td>--...</td>
            </tr><tr>
                <td>8</td>
                <td>---..</td>
            </tr><tr>
                <td>9</td>
                <td>----.</td>
            </tr><tr>
                <td>0</td>
                <td>-----</td>
            </tr>
            <tr>
                <td>. (точка)</td>
                <td>......</td>
            </tr>
            <tr>
                <td>, (запятая)</td>
                <td>.-.-.-</td>
            </tr>
            <tr>
                <td>: (двоеточие)</td>
                <td>---...</td>
            </tr>
            <tr>
                <td>; (точка с запятой)</td>
                <td>-.-.-.</td>
            </tr>
            <tr>
                <td>(</td>
                <td>-.--.-</td>
            </tr>
            <tr>
                <td title="Добавленный символ, отсутствующей в оригигальных таблицах" style="color: #ef551a;">)</td>
                <td>-.--..</td>
            </tr>
            <tr>
                <td>' (Апостроф)</td>
                <td>.----.</td>
            </tr>
            <tr>
                <td>" (кавычки)</td>
                <td>.-..-.</td>
            </tr>
            <tr>
                <td>- (тире)</td>
                <td>-....-</td>
            </tr>
            <tr>
                <td>/ (слеш)</td>
                <td>-..-.</td>
            </tr>
            <tr>
                <td>? (знак вопроса)</td>
                <td>..--..</td>
            </tr>
            <tr>
                <td>! (восклицательный знак)</td>
                <td>--..--</td>
            </tr>
            <tr>
                <td>(знак раздела, пробел)</td>
                <td>-...-</td>
            </tr>
            <tr>
                <td>@</td>
                <td>.--.-.</td>
            </tr>
            <tr>
                <td title="Добавленный символ, отсутствующей в оригигальных таблицах" style="color: #ef551a;">\n (перевод строки)</td>
                <td>-.-..</td>
            </tr>
            <tr>
                <td title="Добавленный символ, отсутствующей в оригигальных таблицах" style="color: #ef551a;">\r (перевод каретки)</td>
                <td>-.-..-</td>
            </tr>
            <tr>
                <td>[END] (конец связи)</td>
                <td>..-.-</td>
            </tr>
            </tbody>
        </table>

        <br><br>

        <i>Примечание.</i> Дизайн сайта был честно подсмотрен на аналогичном сервисе, который кодирует base64.
        <br><br>
        <i>Анектод в тему:</i><br>
        Немцы копали на глубине, соответствующей 18 веку и нашли медные провода. Значит, в Германии уже была телефонная
        связь.<br>
        Американцы копали на глубине 15 века и обнаружили стеклянные фрагменты. Значит, в Америке уже была
        оптоволоконная связь.<br>
        Русские копнули на глубине 11 века и ничего не нашли. Значит, уже в те времена в России пользовались
        беспроводной связью.


        <br><br>
        <img src="static/img/penguin_128.png" title="Хехеей! Привет! :)" onclick="penguin_click()"/>
    </div>

    <div class="exec_time"><?php printf('Время выполнения: %.4F сек.', microtime(true) - $start_time); ?></div>
</div>

<script>
	function penguin_click() {
		alert('Хей! Круто, да? :)');
	}

	function swap_codes() {
		let input_area = document.getElementById('input');
		let output_area = document.getElementById('output');
		let out_tmp = output_area.value;
		output_area.value = input_area.value;
		input_area.value = out_tmp;
	}

	function copy_result() {
		let output_area = document.getElementById('output');
		output_area.select();
		document.execCommand('copy');
		output_area.selectionStart = output_area.selectionEnd;
	}
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function () {
			try {
				w.yaCounter49614481 = new Ya.Metrika2({
					id: 49614481,
					clickmap: true,
					trackLinks: true,
					accurateTrackBounce: true
				});
			} catch (e) {
			}
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () {
				n.parentNode.insertBefore(s, n);
			};
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/tag.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else {
			f();
		}
	})(document, window, "yandex_metrika_callbacks2");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/49614481" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>